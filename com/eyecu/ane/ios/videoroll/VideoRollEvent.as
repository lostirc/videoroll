package com.eyecu.ane.ios.videoroll{

	import flash.events.Event;
	import flash.display.BitmapData;
	import flash.utils.ByteArray;
	
	public class VideoRollEvent extends Event{
		private static var _onVideoSelect:String = "onVideoSelect";
		private static var _onVideoPromise:String = "onVideoPromise";
		private static var _onBatchThumbnailsComplete:String = "onBatchThumbnailsComplete";
		
		public var videoUrl:String;
		public var videoThumbnail:BitmapData;
		public var videoData:ByteArray;
        public var videoLength:Number;
        public var batchVideoThumbUrls:Array;
		
		public static function get ON_VIDEO_SELECT():String{
			return _onVideoSelect
		}
		
		public static function get ON_VIDEO_PROMISE():String{
			return _onVideoPromise;
		}
		
		public static function get ON_BATCH_THUMBNAILS_COMPLETE():String{
			return _onBatchThumbnailsComplete;
		}
		
		public function VideoRollEvent(type:String){
			super(type, false, false);
		}
	
	}
}