
#import "VideoRoll.h"
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import "FlashRuntimeExtensions.h"


VideoRoll *videoRoll;
//ane implmentation

FREObject OpenVideoRoll(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * title;
    uint32_t len;
    FREGetObjectAsUTF8(argv[0], &len, &title);
    uint32_t allowTrim;
    FREGetObjectAsBool(argv[1], &allowTrim);
    int32_t maxTrimLen;
    FREGetObjectAsInt32(argv[2], &maxTrimLen);

    [videoRoll openVideoRoll:title
               withTitleLen:len
               withUseBytes:false
               withAllowTrim:allowTrim
               withTrimLen:maxTrimLen
               withCacheAsset:false];

    return NULL;
}

FREObject OpenVideoRollReturnBytes(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * title;
    uint32_t len;
    FREGetObjectAsUTF8(argv[0], &len, &title);
    uint32_t allowTrim;
    FREGetObjectAsBool(argv[1], &allowTrim);
    uint32_t maxTrimLen;
    FREGetObjectAsUint32(argv[2], &maxTrimLen);
    uint32_t cacheAsset;
    FREGetObjectAsBool(argv[3], &cacheAsset);

    [videoRoll openVideoRoll:title
               withTitleLen:len
               withUseBytes:true
               withAllowTrim:allowTrim
               withTrimLen:maxTrimLen
               withCacheAsset:cacheAsset];


    return NULL;
}

FREObject AskForBinaryVideoData(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [videoRoll acquireVideoBinaryData:argv[0] withBitmmapData:argv[1]];
    return NULL;
}


FREObject GetBitmapFromVideoAtTime(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{

    
    double time;
    FREGetObjectAsDouble(argv[0], &time);
    uint32_t doCleanup;
    FREGetObjectAsBool(argv[2], &doCleanup);
    [videoRoll getBitmapFromVideoAtTime:time withBitmapData:argv[1] doCleanup:doCleanup];
    return NULL;
}

FREObject GetBitmapFromVideoAtTimes(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    double imgWidth;
    double imgHeight;

    FREGetObjectAsDouble(argv[1], &imgWidth);
    FREGetObjectAsDouble(argv[2], &imgHeight);


    [videoRoll getBitmapFromVideoAtTimes:argv[0] thumbWidth:imgWidth thumbHeight:imgHeight];


    return NULL;
}

FREObject init(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    //create a new helper
    if(!videoRoll){
        videoRoll = [[VideoRoll alloc] init];
        [videoRoll setContext:ctx];
    }
    return NULL;
}


//ane conext
void ContextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions)
{
    *numFunctions = 6;
    FRENamedFunction* func = (FRENamedFunction*)malloc(sizeof(FRENamedFunction)*(*numFunctions));
    func[0].name = (const uint8_t*)"openVideoRoll";
    func[0].functionData = NULL;
    func[0].function = &OpenVideoRoll;
    
    func[1].name = (const uint8_t*)"init";
    func[1].functionData = NULL;
    func[1].function = &init;
    
    func[2].name = (const uint8_t*)"openVideoRollReturnBytes";
    func[2].functionData = NULL;
    func[2].function = &OpenVideoRollReturnBytes;
    
    func[3].name = (const uint8_t*)"askForBinaryVideoData";
    func[3].functionData = NULL;
    func[3].function = &AskForBinaryVideoData;
    
    func[4].name = (const uint8_t*)"getBitmapFromVideoAtTime";
    func[4].functionData = NULL;
    func[4].function = &GetBitmapFromVideoAtTime;
    
    func[5].name = (const uint8_t*)"getBitmapFromVideoAtTimes";
    func[5].functionData = NULL;
    func[5].function = &GetBitmapFromVideoAtTimes;

    *functions = func;
    
}


void ContextFinalizer(FREContext ctx)
{
    if(videoRoll){
        [videoRoll release];
    }
    return;
}


void VideoRollExtInitializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer)
{
    *extData = NULL;
    *ctxInitializer = &ContextInitializer;
    *ctxFinalizer   = &ContextFinalizer;
}

void VideoRollFinalizer(void* extData)
{

    return;
}


