#import <Foundation/Foundation.h>
#import <MediaPlayer/MPMediaPickerController.h>
#import <UIKit/UIImagePickerController.h>
#import "FlashRuntimeExtensions.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>

@interface VideoRoll : NSObject<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate>
{
    FREContext _context;
}

-(void) openVideoRoll:(const uint8_t *) pickerTitle
        withTitleLen:(uint32_t) titleLen
        withUseBytes:(BOOL) useBytes
        withAllowTrim:(BOOL) allowTrim
        withTrimLen:(int32_t) trimLen
        withCacheAsset:(BOOL) doCacheAsset;

-(void) acquireVideoBinaryData:(FREObject) videoByteArray withBitmmapData:(FREObject) thumbBitmapData;

-(void) getBitmapFromVideoAtTime:(double) time
        withBitmapData:(FREObject) bitmapData
        doCleanup:(BOOL) cleanup;

-(void) getBitmapFromVideoAtTimes:(FREObject)timesArray
        thumbWidth:(double) width
        thumbHeight:(double) height;

-(void) setContext:(FREContext)ctx;

@end
